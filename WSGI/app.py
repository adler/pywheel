#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time     : 2020/1/7 16:01
# @Author     : chenyao
import os
import sys


def simple_app(environ, start_response):
    status = "200 OK"
    response_header = [("Content-type", "text/html")]
    start_response(status, response_header)
    return [b'hello world! by cy\n']


def wsgi_to_bytes(s):
    return s.encode()


def run_with_cgi(application):
    environ = dict(os.environ.items())
    environ["wsgi.input"] = sys.stdin.buffer
    environ["wsgi.errors"] = sys.stderr
    environ["wsgi.version"] = (1, 0)
    environ["wsgi.multithread"] = False
    environ["wsgi.multiprocess"] = True
    environ["wsgi.run_once"] = True

    if environ.get("HTTPS", "off") in ("on", "1"):
        environ['wsgi.url_scheme'] = "https"
    else:
        environ['wsgi.url_scheme'] = "http"

    headers_set = []
    headers_sent = []

    def write(data):
        out = sys.stdout.buffer
        if not headers_set:
            raise AssertionError("write() before start_response()")
        elif not headers_sent:
            status, response_headers = headers_sent[:] = headers_set
            out.write(wsgi_to_bytes('Status: {}\r\n'.format(status)))
            for header in response_headers:
                out.write(wsgi_to_bytes('{}: {}\r\n'.format(header[0], header[1])))
            out.write(wsgi_to_bytes('\r\n'))

        out.write(data)
        out.flush()

    def start_response(status, response_header, exc_info=None):
        if exc_info:
            try:
                if headers_sent:
                    raise (exc_info[0], exc_info[1], exc_info[2])
            finally:
                exc_info = None
        elif headers_set:
            raise AssertionError("Header already set")

        headers_set[:] = [status, response_header]
        return write

    result = application(environ, start_response)

    try:
        for data in result:
            if data:
                write(data)
            if not headers_set:
                write('')
    finally:
        if hasattr(result, "close"):
            result.close()


if __name__ == "__main__":
    run_with_cgi(simple_app)
