#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time     : 2020/1/7 14:41
# @Author     : chenyao
import time
import errno
import socket
import threading

EOL1 = b'\n\n'
EOL2 = b'\r\n'

body = 'Hello, world! <h1> from the5fire 《Django企业开发实战》</h1>'
response_params = [
    b'HTTP/1.0 200 OK',
    b'Date: Sat, 10 jun 2017 01:01:01 GMT',
    b'Content-Type: text/html; charset=utf-8',
    'Content-Length: {}\r\n'.format(len(body)).encode("utf8"),
    body.encode("utf8")
]

response = b'\r\n'.join(response_params)


def handle_connection(conn, addr):
    time.sleep(1)
    request = b''
    while EOL1 not in request and EOL2 not in request:
        request += conn.recv(1024)
    print(addr, request)
    conn.send(response)
    conn.close()


def main():
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversocket.bind(("127.0.0.1", 8001))
    serversocket.listen(10)
    serversocket.setblocking(0)  # 设置socket为非阻塞的

    try:
        i = 0
        while True:
            try:
                conn, addr = serversocket.accept()
            except socket.error as e:
                if e.args[0] != errno.EAGAIN:
                    raise
                continue
            i += 1
            t = threading.Thread(target=handle_connection, args=(conn, addr), name="thread-%d".format(i))
            t.start()
    finally:
        serversocket.close()


if __name__ == "__main__":
    main()
