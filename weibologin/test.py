#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time     : 2019/12/24 16:02
# @Author     : chenyao

import rsa
import time
from binascii import b2a_hex


def f():
    # print(int("10001", 16))
    servertime = str(int(time.time()))
    nonce = "HY6K9A"
    password = "cy.89757"
    c = bytes("" + "\t".join([servertime, nonce]) + "\n" + password, encoding="utf-8")

    for i in range(5):
        print(i)


def f2():
    key = "EB2A38568661887FA180BDDB5CABD5F21C7BFD59C090CB2D245A87AC253062882729293E5506350508E7F9AA3BB77F4333231490F915F6D63C55FE2F08A49B353F444AD3993CACC02DB784ABBB8E42A9B1BBFFFB38BE18D78E87A0E41B9B8F73A928EE0CCEE1F6739884B9777E4FE9E88A1BBE495927AC4A799B3181D6442443"
    pubkey = rsa.PublicKey(int(key, 16), int("10001", 16))
    servertime = str(int(time.time()))
    nonce = "HY6K9A"
    password = "cy.89757"
    res = rsa.encrypt(bytes("" + "\t".join([servertime, nonce]) + "\n" + password, encoding="utf-8"), pubkey)
    print(res)
    print(b2a_hex(res))


if __name__ == "__main__":
    f()
